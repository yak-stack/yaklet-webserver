package yaklet.webserver.handlers

import dev.yakstack.transport.Login
import org.jooq.impl.DSL
import org.mindrot.jbcrypt.BCrypt
import yaklet.webserver.dbConn.DB_SESSIONS_TB
import yaklet.webserver.dbConn.DB_USER_TB
import yaklet.webserver.dbConn.DbConn
import java.util.*

class UserHandler {
    fun userIdFromToken(conn: DbConn, token: String): String {
        return try {
            conn.sql
                .select(DSL.field("user_id"))
                .from(DB_SESSIONS_TB)
                .where(DSL.field("token").eq(UUID.fromString(token)))
                .fetchOne("user_id", String::class.java) ?: ""
        } catch(_: IllegalArgumentException) {
            ""
        }
    }

    fun getUserIdFromEmail(conn: DbConn, email: String): String = conn
        .sql
        .select(DSL.field("user_id"))
        .from(DB_USER_TB)
        .where(DSL.field("email").eq(email.toLowerCase()))
        .fetchOne("user_id", String::class.java)

    fun userExists(conn: DbConn, email: String) =
        conn.sql.selectCount()
            .from(DB_USER_TB)
            .where(DSL.field("email").eq(email.toLowerCase()))
            .fetchOne(0, Integer::class.java) > 0

    fun passwordCorrect(conn: DbConn, request: Login.LoginRequest): Boolean {
        val hashword = conn.sql.select(DSL.field("hashword"))
            .from(DB_USER_TB)
            .where(DSL.field("email").eq(request.email.toLowerCase()))
            .fetchOne("hashword", String::class.java) ?: ""

        return BCrypt.checkpw(
            request.password,
            hashword.toString()
        )
    }

}
