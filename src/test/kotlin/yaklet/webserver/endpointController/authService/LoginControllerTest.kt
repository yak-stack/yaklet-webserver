package yaklet.webserver.endpointController.authService

import dev.yakstack.transport.Login
import dev.yakstack.transport.Register
import org.junit.Test
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.endpointController.AbstractDatabaseTest

class LoginControllerTest: AbstractDatabaseTest() {

    @Test
    fun testUserDoesNotExist() {
        val conn = provideConn()
        val login = LoginController()
        val request = Login.LoginRequest
            .newBuilder()
            .setEmail("email@glarrien.com")
            .setPassword("password")
            .setDeviceName("test device")
            .build()

        val result = login.process(conn, request)
        assert(result.result == LoginResult.USER_NOT_FOUND)
    }

    @Test
    fun testIncorrectCredentials() {
        val conn = provideConn()
        val loginController = LoginController()

        registerUser(conn, "glarrien@darrien.com", "12345")

        val loginResult = loginController.process(
            conn,
            Login.LoginRequest
                .newBuilder()
                .setEmail("glarrien@darrien.com")
                .setPassword("99999")
                .build()
        )

        assert(loginResult.result == LoginResult.INCORRECT_CREDENTIALS)
    }

    @Test
    fun testCaseInsensitiveEmailLogin() {
        val conn = provideConn()
        val loginController = LoginController()
        val email ="DlAsSeR@dlarrien.dev"

        registerUser(conn, email, "12345")
        val loginResult1 = loginController.process(
            conn,
            Login.LoginRequest
                .newBuilder()
                .setEmail(email.toUpperCase())
                .setPassword("12345")
                .build()
        )

        assert(loginResult1.result == LoginResult.OK)

        val loginResult2 = loginController.process(
            conn,
            Login.LoginRequest
                .newBuilder()
                .setEmail(email.toLowerCase())
                .setPassword("12345")
                .build()
        )

        assert(loginResult2.result == LoginResult.OK)
    }


    private fun registerUser(conn: DbConn, email: String, password: String) {
        val registerController = RegisterController()
        val registerResult = registerController.process(
            conn,
            Register.RegisterRequest
                .newBuilder()
                .setEmail(email)
                .setPassword(password)
                .setFirstName("Glarrien")
                .setLastName("Blasser")
                .build()
        )

        assert(registerResult.result == RegisterResult.OK)
    }
}
