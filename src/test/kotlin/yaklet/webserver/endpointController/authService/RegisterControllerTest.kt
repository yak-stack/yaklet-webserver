package yaklet.webserver.endpointController.authService

import dev.yakstack.transport.Register
import org.junit.Test
import yaklet.webserver.endpointController.AbstractDatabaseTest

class RegisterControllerTest: AbstractDatabaseTest() {
    @Test
    fun testCanRegister() {
        val conn = provideConn()
        val registerController = RegisterController()
        val registerResult = registerController.process(
            conn,
            Register.RegisterRequest
                .newBuilder()
                .setEmail("glasser@darien.com")
                .setPassword("234567")
                .setFirstName("Glarrien")
                .setLastName("Blasser")
                .build()
        )

        assert(registerResult.result == RegisterResult.OK)
    }
}
