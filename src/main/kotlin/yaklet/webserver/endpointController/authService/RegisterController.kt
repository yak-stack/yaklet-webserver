package yaklet.webserver.endpointController.authService

import dev.yakstack.transport.Register
import org.jooq.impl.DSL.field
import org.jooq.impl.DSL.table
import org.mindrot.jbcrypt.BCrypt
import yaklet.webserver.dbConn.DB_UINFO_TB
import yaklet.webserver.dbConn.DB_USER_TB
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.handlers.UserHandler
import java.util.*
import javax.mail.internet.AddressException
import javax.mail.internet.InternetAddress

class RegisterController {
    private val userHandler = UserHandler()

    fun process(conn: DbConn, request: Register.RegisterRequest): RegisterResultResponse {
        return if (userHandler.userExists(conn, request.email)) {
            RegisterResultResponse(
                RegisterResult.USER_ALREADY_EXISTS,
                Register.RegisterResponse.getDefaultInstance()
            )
        } else if (unsetFields(request)) {
            RegisterResultResponse(
                RegisterResult.UNSET_FIELDS,
                Register.RegisterResponse.getDefaultInstance()
            )
        } else if (!validEmail(request)) {
            RegisterResultResponse(
                RegisterResult.INVALID_EMAIL,
                Register.RegisterResponse.getDefaultInstance()
            )
        } else {
            registerUser(conn, request)
            RegisterResultResponse(
                RegisterResult.OK,
                Register.RegisterResponse.getDefaultInstance()
            )
        }
    }

    private fun unsetFields(request: Register.RegisterRequest): Boolean {
        return request.email.isEmpty()
            || request.password.isEmpty()
            || request.firstName.isEmpty()
            || request.lastName.isEmpty()
    }

    private fun validEmail(request: Register.RegisterRequest): Boolean {
        var validEmail = true
        try {
            val emailAddr = InternetAddress(request.email)
            emailAddr.validate()
        } catch (_: AddressException) {
            validEmail = false
        }
        return validEmail
    }

    private fun registerUser(conn: DbConn, request: Register.RegisterRequest) {
        val token = UUID.randomUUID()
        val hashword = BCrypt.hashpw(request.password, BCrypt.gensalt())

        conn.sql.batch(
            conn.sql.insertInto(table(DB_USER_TB))
                .set(field("user_id"), token)
                .set(field("email"), request.email.toLowerCase())
                .set(field("hashword"), hashword),
            conn.sql.insertInto(table(DB_UINFO_TB))
                .set(field("user_id"), token)
                .set(field("first_name"), request.firstName)
                .set(field("last_name"), request.lastName)
        ).execute()
    }

}

enum class RegisterResult {
    USER_ALREADY_EXISTS,
    INVALID_EMAIL,
    UNSET_FIELDS,
    OK
}

data class RegisterResultResponse(val result: RegisterResult, val response: Register.RegisterResponse)
