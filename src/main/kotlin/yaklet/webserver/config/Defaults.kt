package yaklet.webserver.config

// 2 seconds
const val DEFAULT_TIMEOUT = 2L * 1000L
const val DEFAULT_CONNECTIONS = 15
const val DB_HOST = "localhost"
const val DB_PORT = 5432
const val DB_USERNAME = "ttc"
const val DB_PASSWORD = "timetrackerclient"
const val SERVING_PORT = 7575
// Size in characters
const val MAX_NOTE_SIZE = 1024
