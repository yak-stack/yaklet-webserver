package yaklet.webserver.transport

import dev.yakstack.transport.AuthServiceGrpc
import dev.yakstack.transport.Login
import dev.yakstack.transport.Logout
import dev.yakstack.transport.Register
import io.grpc.Status
import io.grpc.stub.StreamObserver
import yaklet.webserver.endpointController.authService.LoginController
import yaklet.webserver.endpointController.authService.LoginResult
import yaklet.webserver.dbConn.ConnectionPool
import yaklet.webserver.dbConn.withConn
import yaklet.webserver.endpointController.authService.LogoutController
import yaklet.webserver.endpointController.authService.LogoutResult
import yaklet.webserver.endpointController.authService.RegisterController
import yaklet.webserver.endpointController.authService.RegisterResult

class AuthService(private val pool: ConnectionPool): AuthServiceGrpc.AuthServiceImplBase() {
    private val login = LoginController()
    private val logout = LogoutController()
    private val register = RegisterController()

    override fun login(
        request: Login.LoginRequest,
        responseObserver: StreamObserver<Login.Token>
    ) {
        withConn(pool) {
            val resultToken = login.process(this.conn, request)
            when (resultToken.result) {
                LoginResult.USER_NOT_FOUND -> responseObserver
                    .onError(
                        Status
                            .NOT_FOUND
                            .withDescription("User not found")
                            .asException()
                    )
                LoginResult.INCORRECT_CREDENTIALS -> responseObserver
                    .onError(
                        Status
                            .UNAUTHENTICATED
                            .withDescription("Email or password incorrect")
                            .asException()
                    )
                LoginResult.OK -> {
                    responseObserver.onNext(resultToken.token)
                    responseObserver.onCompleted()
                }
            }
        }
    }

    override fun logout(
        request: Login.Token,
        responseObserver: StreamObserver<Logout.LogoutResponse>
    ) {
        withConn(pool) {

            when (logout.process(this.conn, request).result) {
                LogoutResult.INVALID_TOKEN -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("Invalid session token")
                        .asException()
                )
                LogoutResult.OK -> {
                    responseObserver.onNext(Logout.LogoutResponse.getDefaultInstance())
                    responseObserver.onCompleted()
                }
            }
        }
    }

    override fun register(
        request: Register.RegisterRequest,
        responseObserver: StreamObserver<Register.RegisterResponse>
    ) {
        withConn(pool) {
            val registerResult = register.process(conn, request)
            when (registerResult.result) {
                RegisterResult.USER_ALREADY_EXISTS -> responseObserver.onError(
                    Status
                        .ALREADY_EXISTS
                        .withDescription("Trying to register user that already exists!")
                        .asException()
                )
                RegisterResult.INVALID_EMAIL -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("Invalid email address")
                        .asException()
                )
                RegisterResult.UNSET_FIELDS -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("All fields are not set")
                        .asException()
                )
                RegisterResult.OK -> {
                    responseObserver.onNext(Register.RegisterResponse.getDefaultInstance())
                    responseObserver.onCompleted()
                }
            }
        }
    }
}

