package yaklet.webserver.handlers

import org.junit.Test
import yaklet.webserver.config.MAX_NOTE_SIZE
import kotlin.streams.asSequence

private const val SOURCE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

class NoteHandlerTest {
    @Test
    fun testNoteIsPreservedBelowMaxLen() {
        val note = randomString(MAX_NOTE_SIZE - 100L)
        assert(NoteHandler().truncateNotes(note) == note)
    }

    @Test
    fun testNoteIsTruncated() {
        val note = randomString(MAX_NOTE_SIZE + 500L)
        val truncatedNote = NoteHandler().truncateNotes(note)
        assert(truncatedNote.length == MAX_NOTE_SIZE)
        assert(truncatedNote == note.substring(0, MAX_NOTE_SIZE))
    }

    private fun randomString(outputStrLength: Long) =
        java.util.Random().ints(outputStrLength, 0, SOURCE.length)
            .asSequence()
            .map(SOURCE::get)
            .joinToString("")
}
