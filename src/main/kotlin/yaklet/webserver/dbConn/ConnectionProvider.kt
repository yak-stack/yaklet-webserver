package yaklet.webserver.dbConn

import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import org.jooq.impl.DSL.constraint
import org.jooq.impl.SQLDataType
import yaklet.webserver.config.DB_HOST
import yaklet.webserver.config.DB_PASSWORD
import yaklet.webserver.config.DB_PORT
import yaklet.webserver.config.DB_USERNAME
import java.sql.Connection
import java.sql.DriverManager
import java.util.logging.Level
import java.util.logging.Logger

open class ConnectionProvider {

    val conn: Connection by lazy { initConnection() }

    companion object {
        private val LOGGER = Logger.getLogger(this::class.java.name)
    }

    protected open fun initConnection(): Connection {
        Class.forName("org.postgresql.Driver")
        return DriverManager.getConnection("jdbc:postgresql://$DB_HOST:$DB_PORT/$DB_NAME",
            DB_USERNAME, DB_PASSWORD)
    }

    fun initTables() {
        val sql = DSL.using(conn, SQLDialect.POSTGRES)
        createUserTable(sql)
        createUserInfoTable(sql)
        createSessionsTable(sql)
        createTaskTable(sql)
        createTimeCommitTable(sql)
        LOGGER.log(Level.FINE, "All databases initialized.")
        sql.close()
    }

    private fun createUserTable(sql: DSLContext) {
        sql.createTableIfNotExists(DB_USER_TB)
            .column("user_id", SQLDataType.UUID.nullable(false))
            .column("email", SQLDataType.VARCHAR.length(EMAIL_LENGTH).nullable(false))
            .column("hashword", SQLDataType.CLOB.nullable(false))
            .constraints(
                constraint().primaryKey("user_id"),
                constraint().unique("email")
            ).execute()

        LOGGER.log(Level.FINEST, "$DB_USER_TB initialized")
    }

    private fun createUserInfoTable(sql: DSLContext) {
        sql.createTableIfNotExists(DB_UINFO_TB)
            .column("user_id", SQLDataType.UUID.nullable(false))
            .column("first_name", SQLDataType.VARCHAR(NAME_LENGTH).nullable(false))
            .column("last_name", SQLDataType.VARCHAR(NAME_LENGTH).nullable(false))
            .constraints(
                constraint().primaryKey("user_id"),
                constraint().unique("user_id"),
                constraint().foreignKey("user_id")
                    .references(DB_USER_TB, "user_id")
            ).execute()

        LOGGER.log(Level.FINEST, "$DB_UINFO_TB initialized")
    }

    private fun createSessionsTable(sql: DSLContext) {
        sql.createTableIfNotExists(DB_SESSIONS_TB)
            .column("id", SQLDataType.BIGINT.identity(true).nullable(false))
            .column("user_id", SQLDataType.UUID.nullable(false))
            .column("token", SQLDataType.UUID.nullable(false))
            .column("device_name", SQLDataType.VARCHAR(DEVICE_NAME_LENGTH).nullable(true))
            .constraints(
                constraint().primaryKey("id"),
                constraint().unique("id")
            ).execute()

        LOGGER.log(Level.FINEST, "$DB_SESSIONS_TB initialized")
    }

    private fun createTaskTable(sql: DSLContext) {
        sql.createTableIfNotExists(DB_TASK_TB)
            .column("task_id", SQLDataType.UUID.nullable(false))
            .column("user_id", SQLDataType.UUID.nullable(false))
            .column("category", SQLDataType.CLOB)
            .column("state", SQLDataType.CLOB)
            .column("task", SQLDataType.CLOB.nullable(false))
            .column("notes", SQLDataType.CLOB)
            .constraints(
                constraint().primaryKey("task_id"),
                constraint().foreignKey("user_id")
                    .references(DB_USER_TB, "user_id")
            ).execute()

        LOGGER.log(Level.FINEST, "$DB_TASK_TB initialized")
    }

    private fun createTimeCommitTable(sql: DSLContext) {
        sql.createTableIfNotExists(DB_TIME_COMMIT_TB)
            .column("time_id", SQLDataType.UUID.nullable(false))
            .column("task_id", SQLDataType.UUID.nullable(false))
            .column("start_time", SQLDataType.TIMESTAMP.nullable(false))
            .column("start_tz", SQLDataType.VARCHAR(TZ_LENGTH).nullable(false))
            .column("end_time", SQLDataType.TIMESTAMP.nullable(false))
            .column("end_tz", SQLDataType.VARCHAR(TZ_LENGTH).nullable(false))
            .constraints(
                constraint().primaryKey("time_id"),
                constraint().foreignKey("task_id")
                    .references(DB_TASK_TB, "task_id")
            ).execute()

        LOGGER.log(Level.FINEST, "$DB_TIME_COMMIT_TB initialized")
    }
}
