package yaklet.webserver.handlers

import dev.yakstack.transport.Timecommit
import org.jooq.impl.DSL.field
import yaklet.webserver.dbConn.DB_TASK_TB
import yaklet.webserver.dbConn.DbConn
import java.util.*

class TaskHandler {
    fun taskIdIsValid(conn: DbConn, taskId: String): Boolean {
        return try {
            conn.sql
                .select(field("task_id"))
                .from(DB_TASK_TB)
                .where(field("task_id").eq(UUID.fromString(taskId)))
                .fetchOne(field("task_id"), String::class.java) != null
        } catch (_: IllegalArgumentException) {
            false
        }
    }

    // TODO Optimize
    fun commitsHaveValidTaskIds(conn: DbConn, commits: Timecommit.TimeCommitBundle): Boolean {
        commits.commitsList.forEach {
           if (!taskIdIsValid(conn, it.taskId)) {
               return false
           }
        }
        return true
    }
}
