package yaklet.webserver.endpointController.timeCommitService

import dev.yakstack.transport.TaskOuterClass
import dev.yakstack.transport.Timecommit
import org.junit.Test
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.endpointController.AbstractDatabaseTest
import java.util.*
import kotlin.random.Random

class UpdateTimeCommitControllerTest: AbstractDatabaseTest() {
    @Test
    fun canUpdateTimeCommits() {
        val conn = provideConn()
        val getTimeCommitController = GetTimeCommitController()
        val setupPair = UserTaskSetup(conn).userAndTaskSetup()
        val timeCommits = getTimeCommits(setupPair.second.taskIdsList[0].taskId)
        val commitIds = addSomeTimeCommits(conn, getTimeCommitBundle(setupPair.first, timeCommits))

        val updateTimeController = UpdateTimeCommitController()
        val updatedCommitsList = commitIds.commitIdsList.map { commitId ->
            Timecommit.TimeCommit.newBuilder()
                .setTaskId(setupPair.second.taskIdsList[0].taskId)
                .setTimeId(commitId.commitId)
                .setStartTime(Random.nextLong(100))
                .setStartTz("Africa/Bujumbura")
                .setEndTime(Random.nextLong(100, 1000000000))
                .setEndTz("Africa/Bujumbura")
                .build()
        }

        val updateTimeCommitResult = updateTimeController.process(
            conn,
            getTimeCommitBundle(setupPair.first, updatedCommitsList)
        )
        assert(updateTimeCommitResult.result == UpdateTimeCommitIdResult.OK)

        val retrievedCommits = getTimeCommitController.process(
            conn,
            TaskOuterClass.TaskId.newBuilder()
                .setTaskId(setupPair.second.taskIdsList[0].taskId)
                .setToken(setupPair.first)
                .build()
        )

        assert(retrievedCommits.result == TimeCommitBundleResult.OK)
        val commitSet = retrievedCommits.timeCommits.commitsList.toSet()
        updatedCommitsList.forEach { commit ->
            assert(commitSet.contains(commit))
        }
    }

    @Test
    fun noUpdateWhenMissingFields() {
        val conn = provideConn()
        val setupPair = UserTaskSetup(conn).userAndTaskSetup()
        val timeCommits = getTimeCommits(setupPair.second.taskIdsList[0].taskId)
        val commitIds = addSomeTimeCommits(conn, getTimeCommitBundle(setupPair.first, timeCommits))

        val updateTimeController = UpdateTimeCommitController()
        val updatedCommitsList = commitIds.commitIdsList.map { commitId ->
            Timecommit.TimeCommit.newBuilder()
                .setTaskId(setupPair.second.taskIdsList[0].taskId)
                .setTimeId(commitId.commitId)
                .build()
        }

        val updateTimeCommitResult = updateTimeController.process(
            conn,
            getTimeCommitBundle(setupPair.first, updatedCommitsList)
        )
        assert(updateTimeCommitResult.result == UpdateTimeCommitIdResult.MISSING_FIELDS)
    }

    @Test
    fun noUpdateWhenBadToken() {
        val conn = provideConn()
        val setupPair = UserTaskSetup(conn).userAndTaskSetup()
        val timeCommits = getTimeCommits(setupPair.second.taskIdsList[0].taskId)
        val commitIds = addSomeTimeCommits(conn, getTimeCommitBundle(setupPair.first, timeCommits))

        val updateTimeController = UpdateTimeCommitController()
        val updatedCommitsList = commitIds.commitIdsList.map { commitId ->
            Timecommit.TimeCommit.newBuilder()
                .setTaskId(setupPair.second.taskIdsList[0].taskId)
                .setTimeId(commitId.commitId)
                .setStartTime(Random.nextLong(100))
                .setStartTz("Africa/Bujumbura")
                .setEndTime(Random.nextLong(100, 1000000000))
                .setEndTz("Africa/Bujumbura")
                .build()
        }

        val updateTimeCommitResultMalformed = updateTimeController.process(
            conn,
            getTimeCommitBundle("glarien.com", updatedCommitsList)
        )
        assert(updateTimeCommitResultMalformed.result == UpdateTimeCommitIdResult.INVALID_TOKEN)

        val updateTimeCommitResultNonexistent = updateTimeController.process(
            conn,
            getTimeCommitBundle(UUID.randomUUID().toString(), updatedCommitsList)
        )
        assert(updateTimeCommitResultNonexistent.result == UpdateTimeCommitIdResult.INVALID_TOKEN)
    }

    @Test
    fun noUpdateBadTimeRange() {
        val conn = provideConn()
        val setupPair = UserTaskSetup(conn).userAndTaskSetup()
        val timeCommits = getTimeCommits(setupPair.second.taskIdsList[0].taskId)
        val commitIds = addSomeTimeCommits(conn, getTimeCommitBundle(setupPair.first, timeCommits))

        val updateTimeController = UpdateTimeCommitController()
        val updatedCommitsList = commitIds.commitIdsList.map { commitId ->
            Timecommit.TimeCommit.newBuilder()
                .setTaskId(setupPair.second.taskIdsList[0].taskId)
                .setTimeId(commitId.commitId)
                .setStartTime(Random.nextLong(100))
                .setStartTz("Africa/Bujumbura")
                .setEndTime(1)
                .setEndTz("Africa/Bujumbura")
                .build()
        }

        val updateTimeCommitResult = updateTimeController.process(
            conn,
            getTimeCommitBundle(setupPair.first, updatedCommitsList)
        )
        assert(updateTimeCommitResult.result == UpdateTimeCommitIdResult.INVALID_TIME_RANGE)
    }

    @Test
    fun noUpdateBadCommitId() {
        val conn = provideConn()
        val setupPair = UserTaskSetup(conn).userAndTaskSetup()
        val timeCommits = getTimeCommits(setupPair.second.taskIdsList[0].taskId)
        val commitIds = addSomeTimeCommits(conn, getTimeCommitBundle(setupPair.first, timeCommits))

        val updateTimeController = UpdateTimeCommitController()
        val updatedCommitsListNonexistent = commitIds.commitIdsList.map {
            Timecommit.TimeCommit.newBuilder()
                .setTaskId(setupPair.second.taskIdsList[0].taskId)
                .setTimeId(UUID.randomUUID().toString())
                .setStartTime(Random.nextLong(100))
                .setStartTz("Africa/Bujumbura")
                .setEndTime(Random.nextLong(100, 1000000000))
                .setEndTz("Africa/Bujumbura")
                .build()
        }

        val updateTimeCommitResultNonexistent = updateTimeController.process(
            conn,
            getTimeCommitBundle(setupPair.first, updatedCommitsListNonexistent)
        )
        assert(updateTimeCommitResultNonexistent.result == UpdateTimeCommitIdResult.INVALID_COMMIT_ID)

        val updatedCommitsListMalformed = commitIds.commitIdsList.map {
            Timecommit.TimeCommit.newBuilder()
                .setTaskId(setupPair.second.taskIdsList[0].taskId)
                .setTimeId("davemachado.dev")
                .setStartTime(Random.nextLong(100))
                .setStartTz("Africa/Bujumbura")
                .setEndTime(Random.nextLong(100, 1000000000))
                .setEndTz("Africa/Bujumbura")
                .build()
        }

        val updateTimeCommitResultMalformed = updateTimeController.process(
            conn,
            getTimeCommitBundle(setupPair.first, updatedCommitsListMalformed)
        )
        assert(updateTimeCommitResultMalformed.result == UpdateTimeCommitIdResult.INVALID_COMMIT_ID)
    }

    private fun getTimeCommitBundle(token: String, timeCommitList: List<Timecommit.TimeCommit>): Timecommit.TimeCommitBundle =
        Timecommit.TimeCommitBundle.newBuilder()
            .setToken(token)
            .addAllCommits(timeCommitList)
            .build()

    private fun addSomeTimeCommits(conn: DbConn, timeCommits: Timecommit.TimeCommitBundle): Timecommit.TimeCommitIdBundle {
        val commitTimeResult = CommitTimeController().process(conn, timeCommits)
        assert(commitTimeResult.result == TimeCommitIdResult.OK)
        return commitTimeResult.idBundle
    }

    private fun getTimeCommits(taskId: String) = listOf(
        Timecommit.TimeCommit.newBuilder()
            .setTaskId(taskId)
            .setStartTime(909090)
            .setStartTz("America/North_Dakota/New_Salem")
            .setEndTime(10101010)
            .setEndTz("America/Port-au-Prince")
            .build(),
        Timecommit.TimeCommit.newBuilder()
            .setTaskId(taskId)
            .setStartTime(100000)
            .setStartTz("America/New_York")
            .setEndTime(11000000)
            .setEndTz("America/New_York")
            .build()
    )
}
