package yaklet.webserver.dbConn.connectionFactory

import java.sql.Connection

class MockConnectionFactory: ConnectionFactory {
    override fun getConnection(): Connection = MockConnection()
}
