package yaklet.webserver.endpointController.taskService

import dev.yakstack.transport.Login
import dev.yakstack.transport.TaskOuterClass
import org.jooq.impl.DSL.field
import yaklet.webserver.dbConn.DB_TASK_TB
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.handlers.UserHandler
import java.util.*

class GetTasksController {
    private val userHandler = UserHandler()

    fun process(conn: DbConn, request: Login.Token): GetTasksResult {
        val userId = userHandler.userIdFromToken(conn, request.token)
        return when {
            userId.isEmpty() -> GetTasksResult(
                TaskBundleResult.INVALID_TOKEN,
                TaskOuterClass.TaskBundle.getDefaultInstance()
            )
            else -> {
                GetTasksResult(
                    TaskBundleResult.OK,
                    getTasks(conn, UUID.fromString(userId))
                )
            }
        }
    }

    private fun getTasks(conn: DbConn, userId: UUID): TaskOuterClass.TaskBundle {
        val taskRecords = conn.sql
            .select()
            .from(DB_TASK_TB)
            .where(field("user_id").eq(userId))
            .fetch()

        return TaskOuterClass.TaskBundle.newBuilder().addAllTasks(
            taskRecords.map { taskRecord ->
                TaskOuterClass.Task.newBuilder()
                    .setCategory(taskRecord["category"].toString())
                    .setTask(taskRecord["task"].toString())
                    .setState(taskRecord["state"].toString())
                    .setNotes(taskRecord["notes"].toString())
                    .setTaskId(taskRecord["task_id"].toString())
                    .build()
            }
        ).build()
    }
}

enum class TaskBundleResult {
    INVALID_TOKEN,
    OK
}

data class GetTasksResult(val result: TaskBundleResult, val taskBundle: TaskOuterClass.TaskBundle)
