package yaklet.webserver.dbConn.connectionFactory

import java.sql.Connection

interface ConnectionFactory {
    fun getConnection(): Connection
}

