package yaklet.webserver.endpointController.authService

import dev.yakstack.transport.Login
import org.jooq.impl.DSL.field
import org.jooq.impl.DSL.table
import yaklet.webserver.dbConn.DB_SESSIONS_TB
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.handlers.UserHandler
import java.util.*

class LoginController {
    private val userHandler = UserHandler()

    fun process(conn: DbConn, request: Login.LoginRequest): LoginResultToken {
        return if (!userHandler.userExists(conn, request.email)) {
            LoginResultToken(
                LoginResult.USER_NOT_FOUND,
                Login.Token.getDefaultInstance()
            )
        } else if (!userHandler.passwordCorrect(conn, request)) {
            LoginResultToken(
                LoginResult.INCORRECT_CREDENTIALS,
                Login.Token.getDefaultInstance()
            )
        } else LoginResultToken(
            LoginResult.OK,
            initSession(conn, request)
        )
    }

    private fun initSession(conn: DbConn, request: Login.LoginRequest): Login.Token {
        val userId = userHandler.getUserIdFromEmail(conn, request.email)
        val token = UUID.randomUUID()
        conn.sql.insertInto(table(DB_SESSIONS_TB))
            .set(field("user_id"), UUID.fromString(userId))
            .set(field("token"), token)
            .set(field("device_name"), request.deviceName)
            .execute()

        return Login.Token.newBuilder()
            .setToken(token.toString())
            .build()
    }
}

enum class LoginResult {
    USER_NOT_FOUND,
    INCORRECT_CREDENTIALS,
    OK
}

data class LoginResultToken(val result: LoginResult, val token: Login.Token)
