package yaklet.webserver.handlers

import org.jooq.impl.DSL
import yaklet.webserver.dbConn.DB_SESSIONS_TB
import yaklet.webserver.dbConn.DbConn
import java.util.*

class TokenHandler {
    fun tokenIsValid(conn: DbConn, token: String): Boolean {
        return try {
            conn.sql
                .select(DSL.field("user_id"))
                .from(DB_SESSIONS_TB)
                .where(DSL.field("token").eq(UUID.fromString(token)))
                .fetchOne("user_id", String::class.java) != null
        } catch(_: IllegalArgumentException) {
            false
        }
    }
}
