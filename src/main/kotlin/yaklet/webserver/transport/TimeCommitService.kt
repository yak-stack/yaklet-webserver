package yaklet.webserver.transport

import dev.yakstack.transport.TaskOuterClass
import dev.yakstack.transport.TimeCommitServiceGrpc
import dev.yakstack.transport.Timecommit
import io.grpc.Status
import io.grpc.stub.StreamObserver
import yaklet.webserver.dbConn.ConnectionPool
import yaklet.webserver.dbConn.withConn
import yaklet.webserver.endpointController.timeCommitService.CommitTimeController
import yaklet.webserver.endpointController.timeCommitService.DeleteTimeCommitController
import yaklet.webserver.endpointController.timeCommitService.DeleteTimeCommitResult
import yaklet.webserver.endpointController.timeCommitService.GetTimeCommitController
import yaklet.webserver.endpointController.timeCommitService.TimeCommitBundleResult
import yaklet.webserver.endpointController.timeCommitService.TimeCommitIdResult
import yaklet.webserver.endpointController.timeCommitService.UpdateTimeCommitController
import yaklet.webserver.endpointController.timeCommitService.UpdateTimeCommitIdResult

class TimeCommitService(private val pool: ConnectionPool): TimeCommitServiceGrpc.TimeCommitServiceImplBase() {
    private val commitTimeController = CommitTimeController()
    private val getTimeCommitController = GetTimeCommitController()
    private val updateTimeCommitController = UpdateTimeCommitController()
    private val deleteTimeCommitController = DeleteTimeCommitController()

    override fun commitTime(
        request: Timecommit.TimeCommitBundle,
        responseObserver: StreamObserver<Timecommit.TimeCommitIdBundle>
    ) {
        withConn(pool) {
            val timeCommitResult = commitTimeController.process(conn, request)
            when (timeCommitResult.result) {
                TimeCommitIdResult.MISSING_FIELDS -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("Missing fields in timecommits. " +
                            "Ensure all fields are filled out.")
                        .asException()
                )
                TimeCommitIdResult.INVALID_TOKEN -> responseObserver.onError(
                    Status
                        .UNAUTHENTICATED
                        .withDescription("Invalid token. Log out and log in again.")
                        .asException()
                )
                TimeCommitIdResult.INVALID_TIME_RANGE -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("TimeCommits have end value set before beginning value")
                        .asException()
                )
                TimeCommitIdResult.INVALID_TIME_ZONE -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("Invalid Timezone. Please ensure you are using " +
                            "values from the timezone database " +
                            "https://en.wikipedia.org/wiki/List_of_tz_database_time_zones#List")
                        .asException()
                )
                TimeCommitIdResult.INVALID_TASK_ID -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("Attempting to add TimeCommit(s) to nonexistent task")
                        .asException()
                )
                TimeCommitIdResult.OK -> {
                    responseObserver.onNext(timeCommitResult.idBundle)
                    responseObserver.onCompleted()
                }
            }
        }
    }

    override fun updateTimeCommit(
        request: Timecommit.TimeCommitBundle,
        responseObserver: StreamObserver<Timecommit.TimeCommitIdBundle>
    ) {
        withConn(pool) {
            val updateTimeCommitsResult = updateTimeCommitController.process(conn, request)
            when (updateTimeCommitsResult.result) {
                UpdateTimeCommitIdResult.MISSING_FIELDS -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("Missing fields in updated timecommits. " +
                            "Make sure to fill out all fields.")
                        .asException()
                )
                UpdateTimeCommitIdResult.INVALID_TIME_RANGE -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("TimeCommits have end value set before beginning value")
                        .asException()
                )
                UpdateTimeCommitIdResult.INVALID_TIME_ZONE -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("Invalid Timezone. Please ensure you are using " +
                            "values from the timezone database " +
                            "https://en.wikipedia.org/wiki/List_of_tz_database_time_zones#List")
                        .asException()
                )
                UpdateTimeCommitIdResult.INVALID_TOKEN -> responseObserver.onError(
                    Status
                        .UNAUTHENTICATED
                        .withDescription("Invalid token. Log out and log in again.")
                        .asException()
                )
                UpdateTimeCommitIdResult.INVALID_COMMIT_ID -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("Attempting to update timecommit with " +
                            "nonexistent or malformed time_id")
                        .asException()
                )
                UpdateTimeCommitIdResult.OK -> {
                    responseObserver.onNext(updateTimeCommitsResult.idBundle)
                    responseObserver.onCompleted()
                }
            }
        }
    }

    override fun getTimeCommits(
        request: TaskOuterClass.TaskId,
        responseObserver: StreamObserver<Timecommit.TimeCommitBundle>
    ) {
        withConn(pool) {
            val getTimeCommitsResult = getTimeCommitController.process(conn, request)
            when (getTimeCommitsResult.result) {
                TimeCommitBundleResult.INVALID_TOKEN -> responseObserver.onError(
                    Status
                        .UNAUTHENTICATED
                        .withDescription("Invalid token. Log out and log in again.")
                        .asException()
                )
                TimeCommitBundleResult.INVALID_TASK_ID -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("Invalid taskId or corrupt taskId. " +
                            "Please use a valid one.")
                        .asException()
                )
                TimeCommitBundleResult.OK -> {
                    responseObserver.onNext(getTimeCommitsResult.timeCommits)
                    responseObserver.onCompleted()
                }
            }
        }
    }

    override fun deleteTimeCommit(
        request: Timecommit.TimeCommitId,
        responseObserver: StreamObserver<Timecommit.TimeCommitDeletionResponse>
    ) {

        withConn(pool) {
            when (deleteTimeCommitController.process(conn, request)) {
                DeleteTimeCommitResult.INVALID_TOKEN -> responseObserver.onError(
                    Status
                        .UNAUTHENTICATED
                        .withDescription("Invalid token. Log out and log back in")
                        .asException()
                )
                DeleteTimeCommitResult.INVALID_COMMIT_ID -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("Attempting to delete TimeCommit that does not exist")
                        .asException()
                )
                else -> {
                    responseObserver.onNext(Timecommit.TimeCommitDeletionResponse.getDefaultInstance())
                    responseObserver.onCompleted()
                }
            }
        }
    }
}

