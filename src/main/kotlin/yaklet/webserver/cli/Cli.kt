package yaklet.webserver.cli

import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.HelpFormatter
import org.apache.commons.cli.Option
import org.apache.commons.cli.OptionGroup
import org.apache.commons.cli.Options
import org.apache.commons.cli.ParseException
import java.util.logging.Level
import java.util.logging.Logger
import kotlin.system.exitProcess

class Cli(private val args: Array<String>) {
    companion object {
        private val LOGGER = Logger.getLogger(this::class.java.name)
    }
    val options = Options()
    init {
        options.addOption("h", "help", false, "Show help")
            .addOption(Option("c", "certChain", true, "Cert chain"))
            .addOption(Option("p", "privateKey", true, "Private key"))
    }

    fun parse(): Pair<String?, String?> {
        return try {
            val cmd = DefaultParser()
            val parsed = cmd.parse(options, args)
            if (parsed.hasOption("h")) {
                help()
                exitProcess(0)
            } else if (parsed.hasOption("c") && parsed.hasOption("p")) {
                Pair(parsed.getOptionValue("c"), parsed.getOptionValue(("p")))
            } else {
                if (parsed.hasOption("c") || parsed.hasOption("p")) {
                    LOGGER.log(Level.WARNING, "certChain and privateKey must be passed for TLS. " +
                        "Continuing unauthenticated.")
                }
                Pair(null, null)
            }

        } catch (e: ParseException) {
            LOGGER.log(Level.SEVERE, e.message)
            help()
            exitProcess(0)
        }
    }

    private fun help(): Pair<String?, String?> {
        HelpFormatter().printHelp("yaklet-webserver", options)
        return Pair(null, null)
    }
}
