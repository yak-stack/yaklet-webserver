package yaklet.webserver.endpointController.taskService

import dev.yakstack.transport.TaskOuterClass
import org.jooq.impl.DSL.field
import org.jooq.impl.DSL.table
import yaklet.webserver.dbConn.DB_TASK_TB
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.handlers.TaskHandler
import yaklet.webserver.handlers.UserHandler
import java.util.*

class DeleteTaskController {
    private val userHandler = UserHandler()
    private val taskHandler = TaskHandler()

    fun process(conn: DbConn, request: TaskOuterClass.TaskId): TaskDeletedResult {
        val userId = userHandler.userIdFromToken(conn, request.token)
        return when {
            userId.isEmpty() -> TaskDeletedResult(DeleteTaskResult.INVALID_TOKEN)
            !taskHandler.taskIdIsValid(conn, request.taskId) -> TaskDeletedResult(DeleteTaskResult.INVALID_TASK_ID)
            else -> {
                deleteTask(conn, request, UUID.fromString(userId))
                TaskDeletedResult(DeleteTaskResult.OK)
            }
        }
    }

    private fun deleteTask(conn: DbConn, request: TaskOuterClass.TaskId, userId: UUID) {
        conn.sql.deleteFrom(table(DB_TASK_TB))
            .where(
                field("task_id").eq(UUID.fromString(request.taskId)),
                field("user_id").eq(userId)
            ).execute()
    }
}

enum class DeleteTaskResult {
    INVALID_TOKEN,
    INVALID_TASK_ID,
    OK
}

class TaskDeletedResult(val result: DeleteTaskResult)
