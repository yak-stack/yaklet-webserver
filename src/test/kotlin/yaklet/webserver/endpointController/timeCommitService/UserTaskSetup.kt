package yaklet.webserver.endpointController.timeCommitService

import dev.yakstack.transport.Login
import dev.yakstack.transport.Register
import dev.yakstack.transport.TaskOuterClass
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.endpointController.authService.LoginController
import yaklet.webserver.endpointController.authService.LoginResult
import yaklet.webserver.endpointController.authService.RegisterController
import yaklet.webserver.endpointController.taskService.SubmitTaskController
import yaklet.webserver.endpointController.taskService.TaskIdBundleResult

/**
 * For setting up a user, registering and logging in,
 * then making one task.
 * This is for tests that need a small environment set up, but don't
 * actually care about the contents of any of the setup.
 * @return a token for anything requiring auth and a taskIdBundle.
 */
class UserTaskSetup(val conn: DbConn) {
    fun userAndTaskSetup(): Pair<String, TaskOuterClass.TaskIdBundle> {
        val token = setupUserAndLogin()

        return Pair(token, submitTaskBundle(getTaskBundle(token, getTaskList())))
    }

    private fun submitTaskBundle(taskBundle: TaskOuterClass.TaskBundle): TaskOuterClass.TaskIdBundle {
        val submitTaskResult = SubmitTaskController().process(conn, taskBundle)
        assert(submitTaskResult.result == TaskIdBundleResult.OK)
        return submitTaskResult.idBundle
    }

    private fun setupUserAndLogin(): String {
        val registerController = RegisterController()
        val loginController = LoginController()

        val email = "glarrien@darrien.com"
        val password = "hungrybubber159"

        registerController.process(
            conn,
            Register.RegisterRequest
                .newBuilder()
                .setEmail(email)
                .setPassword(password)
                .setFirstName("glarrien")
                .setLastName("dasser")
                .build()
        )

        val loginResult = loginController.process(
            conn,
            Login.LoginRequest
                .newBuilder()
                .setEmail(email)
                .setPassword(password)
                .setDeviceName("some kinda test device idk")
                .build()
        )

        assert(loginResult.result == LoginResult.OK)

        return loginResult.token.token
    }

    private fun getTaskBundle(token: String, taskList: List<TaskOuterClass.Task>) = TaskOuterClass.TaskBundle.newBuilder()
        .setToken(token)
        .addAllTasks(taskList)
        .build()

    private fun getTaskList() = listOf(
        TaskOuterClass.Task.newBuilder()
            .setCategory("Testing weee")
            .setState("IN-PROGRESS")
            .setTask("Write some tests")
            .setNotes("blah blah blah I guess we need tests")
            .build(),
        TaskOuterClass.Task.newBuilder()
            .setCategory("Testing weee")
            .setState("TODO")
            .setTask("Write tests for this whole gosh darn application")
            .setNotes("Boy do I wish these wrote themselves")
            .build(),
        TaskOuterClass.Task.newBuilder()
            .setCategory("Something actually fun")
            .setState("DONE")
            .setTask("Give Vrinda a hug")
            .setNotes("There's a time when the operation of the machine becomes so odious...")
            .build()
    )
}
