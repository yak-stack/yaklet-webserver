package yaklet.webserver.dbConn

import java.sql.Connection
import java.sql.DriverManager

class MockConnectionProvider(
    private val jdbcUrl: String,
    private val username: String,
    private val password: String
): ConnectionProvider() {
    override fun initConnection(): Connection {
        return DriverManager.getConnection(jdbcUrl, username, password)
    }
}
