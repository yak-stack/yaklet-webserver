package yaklet.webserver.dbConn

import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import yaklet.webserver.dbConn.dslFactory.DSLFactory
import java.lang.Exception
import java.sql.Connection

class DbConn(dslFactory: DSLFactory) {
    val sql: DSLContext = dslFactory.getDSL()
}

