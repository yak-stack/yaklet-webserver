# TIMETRACKER-BACKEND (BETA PROJECT)

This timetracker-backend is an beta backend client for my time tracker. It
implements the API described here: [here](https://gitlab.com/yak-stack/proto-yak)

The API uses GRPC, and as such you'll need a GRPC client for your language to
send requests.

The service has many tests, but has not had much manual use. It is very much a
beta project.

Let's set it up.

# timetracker-backend

Want it now? The binaries are too big to store in GitLab (fuck you spring) but
building it only requires a machine running *Java* and *PostgresQL*.

First the SQL database needs to be created.

There's a setup script available in /setup/ or you can run this:

```sql
CREATE USER ttc WITH PASSWORD 'timetrackerclient';
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO ttc;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO ttc;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE DATABASE timedb;
```

If you want to test the setup, you can run `./gradlew run` which will begin
running the app and end when you hit CTRL + C. Retrieving a binary is preferable
with will make startup time faster though.

```bash
$ ./gradlew distZip
```

This will create a zip in `/build/distribution` which contains the "binary" and
dependencies needed to run.

When you want to run it, unzip the zip file, and run the "binary" in /bin:
```bash
./timetracker-backend
```

It will start on port 7575. From there, you're done!

# Contributing

All code is expected to pass through the default [detekt](https://github.com/shyiko/ktlint) settings.

You can test with:

```bash
$ ./gradlew detekt
```

Tests are written to use Docker containers, so Docker must be installed on the
host system to run, otherwise many tests will fail.

The host system must also have at least 2Gi of storage.

NOTE: JOOQ reflective access warnings can be ignored for now. Release 3.12 is
confirmed to fix the warnings with Java releases >9, but has not yet been
released.
