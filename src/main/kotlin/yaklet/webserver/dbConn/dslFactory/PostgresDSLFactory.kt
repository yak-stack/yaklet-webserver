package yaklet.webserver.dbConn.dslFactory

import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import java.sql.Connection

class PostgresDSLFactory(private val conn: Connection): DSLFactory {
    override fun getDSL(): DSLContext = DSL.using(conn, SQLDialect.POSTGRES)

}
