package yaklet.webserver.endpointController.taskService

import dev.yakstack.transport.Login
import dev.yakstack.transport.Register
import dev.yakstack.transport.TaskOuterClass
import org.junit.Test
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.endpointController.AbstractDatabaseTest
import yaklet.webserver.endpointController.authService.LoginController
import yaklet.webserver.endpointController.authService.LoginResult
import yaklet.webserver.endpointController.authService.RegisterController
import java.util.*

class GetTasksControllerTest: AbstractDatabaseTest() {

    @Test
    fun testCanGetTasks() {
        val conn = provideConn()
        val getTasksController = GetTasksController()

        val token = setupUserAndLogin(conn)
        val taskList = getTaskList()
        val taskBundle = getTaskBundle(token, getTaskList())
        val submitTaskResult = SubmitTaskController().process(conn, taskBundle)
        assert(submitTaskResult.result == TaskIdBundleResult.OK)


        val receivedTasks = getTasksController.process(
            conn,
            Login.Token.newBuilder()
                .setToken(token)
                .build()
        )

        assert(receivedTasks.result == TaskBundleResult.OK)
        assert(receivedTasks.taskBundle.tasksList.size == taskList.size)
        for ((idx, task) in receivedTasks.taskBundle.tasksList.withIndex()) {
            assert(task.category == taskList[idx].category)
            assert(task.state == taskList[idx].state)
            assert(task.task == taskList[idx].task)
            assert(task.notes == taskList[idx].notes)
        }
    }

    @Test
    fun testInaccessibleWithBadToken() {
        val conn = provideConn()
        val getTasksController = GetTasksController()

        val token = setupUserAndLogin(conn)
        val taskBundle = getTaskBundle(token, getTaskList())
        val submitTaskResult = SubmitTaskController().process(conn, taskBundle)
        assert(submitTaskResult.result == TaskIdBundleResult.OK)

        val receivedTasks = getTasksController.process(
            conn,
            Login.Token.newBuilder()
                .setToken(token + "invalid")
                .build()
        )

        assert(receivedTasks.result == TaskBundleResult.INVALID_TOKEN)
    }

    private fun getTaskList() = listOf(
        TaskOuterClass.Task.newBuilder()
            .setCategory("Testing weee")
            .setState("IN-PROGRESS")
            .setTask("Write some tests")
            .setNotes("blah blah blah I guess we need tests")
            .build(),
        TaskOuterClass.Task.newBuilder()
            .setCategory("Testing weee")
            .setState("TODO")
            .setTask("Write tests for this whole gosh darn application")
            .setNotes("Boy do I wish these wrote themselves")
            .build(),
        TaskOuterClass.Task.newBuilder()
            .setCategory("Something actually fun")
            .setState("DONE")
            .setTask("Give Vrinda a hug")
            .setNotes("She is a bean")
            .build()
    )

    private fun getTaskBundle(token: String, taskList: List<TaskOuterClass.Task>) = TaskOuterClass.TaskBundle.newBuilder()
        .setToken(token)
        .addAllTasks(taskList)
        .build()

    private fun setupUserAndLogin(conn: DbConn): String {
        val registerController = RegisterController()
        val loginController = LoginController()

        val email = "glarrien@darrien.com"
        val password = "hungrybubber159"

        registerController.process(
            conn,
            Register.RegisterRequest
                .newBuilder()
                .setEmail(email)
                .setPassword(password)
                .setFirstName("glarrien")
                .setLastName("dasser")
                .build()
        )

        val loginResult = loginController.process(
            conn,
            Login.LoginRequest
                .newBuilder()
                .setEmail(email)
                .setPassword(password)
                .setDeviceName("some kinda test device idk")
                .build()
        )

        assert(loginResult.result == LoginResult.OK)

        return loginResult.token.token
    }
}
