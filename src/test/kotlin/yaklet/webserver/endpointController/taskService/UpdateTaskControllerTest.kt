package yaklet.webserver.endpointController.taskService

import dev.yakstack.transport.Login
import dev.yakstack.transport.Register
import dev.yakstack.transport.TaskOuterClass
import org.junit.Test
import yaklet.webserver.dbConn.DbConn
import yaklet.webserver.endpointController.AbstractDatabaseTest
import yaklet.webserver.endpointController.authService.LoginController
import yaklet.webserver.endpointController.authService.LoginResult
import yaklet.webserver.endpointController.authService.RegisterController
import java.util.*

class UpdateTaskControllerTest: AbstractDatabaseTest() {
    @Test
    fun canUpdateTask() {
        val conn = provideConn()
        val updateTaskController = UpdateTaskController()

        val token = setupUserAndLogin(conn)
        val taskList = getTaskList()
        val taskIdBundle = submitTasks(
            conn,
            getTaskBundle(token, taskList)
        )

        val taskToUpdate = TaskOuterClass.Task.newBuilder()
            .setTaskId(taskIdBundle.taskIdsList[0].taskId)
            .setToken(token)
            .setCategory("Testing updates")
            .setState("Doing it right now tho")
            .setTask(taskList[0].task)
            .setNotes(taskList[0].notes)
            .build()

        val updateTaskResult = updateTaskController.process(conn, taskToUpdate)
        assert(updateTaskResult.result == TaskIdResult.OK)

        val retrievedTasks = GetTasksController().process(
            conn,
            Login.Token.newBuilder().setToken(token).build()
        )

        // Need to add the token into the comparison, otherwise assert will fail
        assert(
            retrievedTasks.taskBundle.tasksList[0]
                .toBuilder()
                .setToken(token)
                .build() == taskToUpdate
        )
    }

    @Test
    fun needsToken() {
        val conn = provideConn()
        val updateTaskController = UpdateTaskController()

        val token = setupUserAndLogin(conn)
        val taskList = getTaskList()
        val taskIdBundle = submitTasks(
            conn,
            getTaskBundle(token, taskList)
        )

        // Do not set token
        val taskToUpdate = TaskOuterClass.Task.newBuilder()
            .setTaskId(taskIdBundle.taskIdsList[0].taskId)
            .setCategory("Testing updates")
            .setState("Doing it right now tho")
            .setTask(taskList[0].task)
            .setNotes(taskList[0].notes)
            .build()

        val updateTaskResult = updateTaskController.process(conn, taskToUpdate)
        assert(updateTaskResult.result == TaskIdResult.INVALID_TOKEN)
    }

    @Test
    fun invalidToken() {
        val conn = provideConn()
        val updateTaskController = UpdateTaskController()

        val token = setupUserAndLogin(conn)
        val taskList = getTaskList()
        val taskIdBundle = submitTasks(
            conn,
            getTaskBundle(token, taskList)
        )

        // Do not set token
        val taskToUpdate = TaskOuterClass.Task.newBuilder()
            .setTaskId(taskIdBundle.taskIdsList[0].taskId)
            .setToken("hubgrybuber")
            .setCategory("Testing updates")
            .setState("Doing it right now tho")
            .setTask(taskList[0].task)
            .setNotes(taskList[0].notes)
            .build()

        val updateTaskResult = updateTaskController.process(conn, taskToUpdate)
        assert(updateTaskResult.result == TaskIdResult.INVALID_TOKEN)
    }

    @Test
    fun updateWithBadTaskId() {
        val conn = provideConn()
        val updateTaskController = UpdateTaskController()

        val token = setupUserAndLogin(conn)
        val taskList = getTaskList()

        val taskToUpdateMalformed = TaskOuterClass.Task.newBuilder()
            .setTaskId("davemachado.dev")
            .setToken(token)
            .setCategory("Testing updates")
            .setState("Doing it right now tho")
            .setTask(taskList[0].task)
            .setNotes(taskList[0].notes)
            .build()

        val malformedUpdateTaskResult = updateTaskController.process(conn, taskToUpdateMalformed)
        assert(malformedUpdateTaskResult.result == TaskIdResult.INVALID_TASK_ID)

        val taskToUpdateNonexistent = TaskOuterClass.Task.newBuilder()
        .setTaskId(UUID.randomUUID().toString())
            .setToken(token)
            .setCategory("Testing updates")
            .setState("Doing it right now tho")
            .setTask(taskList[0].task)
            .setNotes(taskList[0].notes)
            .build()

        val nonexistentUpdateTaskResult = updateTaskController.process(conn, taskToUpdateNonexistent)
        assert(nonexistentUpdateTaskResult.result == TaskIdResult.INVALID_TASK_ID)
    }

    @Test
    fun removeCriticalFields() {
        val conn = provideConn()
        val updateTaskController = UpdateTaskController()

        val token = setupUserAndLogin(conn)
        val taskList = getTaskList()
        val taskIdBundle = submitTasks(
            conn,
            getTaskBundle(token, taskList)
        )

        // Pretty much remove everything except TaskId and token
        val taskToUpdate = TaskOuterClass.Task.newBuilder()
            .setTaskId(taskIdBundle.taskIdsList[0].taskId)
            .setToken(token)
            .build()

        val updateTaskResult = updateTaskController.process(conn, taskToUpdate)
        assert(updateTaskResult.result == TaskIdResult.MISSING_FIELDS)
    }

    private fun getTaskList() = listOf(
        TaskOuterClass.Task.newBuilder()
            .setCategory("Testing weee")
            .setState("IN-PROGRESS")
            .setTask("Write some tests")
            .setNotes("blah blah blah I guess we need tests")
            .build()
    )

    private fun getTaskBundle(token: String, taskList: List<TaskOuterClass.Task>) = TaskOuterClass.TaskBundle.newBuilder()
        .setToken(token)
        .addAllTasks(taskList)
        .build()

    private fun setupUserAndLogin(conn: DbConn): String {
        val registerController = RegisterController()
        val loginController = LoginController()

        val email = "glarrien@darrien.com"
        val password = "hungrybubber159"

        registerController.process(
            conn,
            Register.RegisterRequest
                .newBuilder()
                .setEmail(email)
                .setPassword(password)
                .setFirstName("glarrien")
                .setLastName("dasser")
                .build()
        )

        val loginResult = loginController.process(
            conn,
            Login.LoginRequest
                .newBuilder()
                .setEmail(email)
                .setPassword(password)
                .setDeviceName("some kinda test device idk")
                .build()
        )

        assert(loginResult.result == LoginResult.OK)

        return loginResult.token.token
    }

    private fun submitTasks(conn: DbConn, taskBundle: TaskOuterClass.TaskBundle): TaskOuterClass.TaskIdBundle {
        val taskBundleResult = SubmitTaskController().process(conn, taskBundle)
        assert(taskBundleResult.result == TaskIdBundleResult.OK)

        return taskBundleResult.idBundle
    }
}
