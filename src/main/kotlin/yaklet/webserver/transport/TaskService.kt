package yaklet.webserver.transport

import dev.yakstack.transport.Login
import dev.yakstack.transport.TaskOuterClass
import dev.yakstack.transport.TaskServiceGrpc
import io.grpc.Status
import io.grpc.stub.StreamObserver
import yaklet.webserver.dbConn.ConnectionPool
import yaklet.webserver.dbConn.withConn
import yaklet.webserver.endpointController.taskService.DeleteTaskController
import yaklet.webserver.endpointController.taskService.DeleteTaskResult
import yaklet.webserver.endpointController.taskService.GetTasksController
import yaklet.webserver.endpointController.taskService.SubmitTaskController
import yaklet.webserver.endpointController.taskService.TaskBundleResult
import yaklet.webserver.endpointController.taskService.TaskIdBundleResult
import yaklet.webserver.endpointController.taskService.TaskIdResult
import yaklet.webserver.endpointController.taskService.UpdateTaskController

class TaskService(private val pool: ConnectionPool): TaskServiceGrpc.TaskServiceImplBase() {
    private val submitTask = SubmitTaskController()
    private val getTasks = GetTasksController()
    private val updateTask = UpdateTaskController()
    private val deleteTask = DeleteTaskController()

    override fun submitTask(
        request: TaskOuterClass.TaskBundle,
        responseObserver: StreamObserver<TaskOuterClass.TaskIdBundle>
    ) {
        withConn(pool) {
            val submitTaskResult = submitTask.process(conn, request)
            when (submitTaskResult.result) {
                TaskIdBundleResult.INVALID_TOKEN -> responseObserver.onError(
                    Status
                        .PERMISSION_DENIED
                        .withDescription("Token is invalid or has expired. Log in again.")
                        .asException()
                )
                TaskIdBundleResult.MISSING_FIELDS -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("Not all fields are filled out. " +
                            "As of now, the fields [task], must be filled out.")
                        .asException()
                )
                TaskIdBundleResult.OK -> {
                    responseObserver.onNext(submitTaskResult.idBundle)
                    responseObserver.onCompleted()
                }
            }
        }
    }

    override fun getTasks(
        request: Login.Token,
        responseObserver: StreamObserver<TaskOuterClass.TaskBundle>
    ) {
        withConn(pool) {
            val getTasksResult = getTasks.process(conn, request)
            when (getTasksResult.result) {
                TaskBundleResult.INVALID_TOKEN -> responseObserver.onError(
                    Status
                        .PERMISSION_DENIED
                        .withDescription("Token is invalid or has expired. Log in again.")
                        .asException()
                )
                TaskBundleResult.OK -> {
                    responseObserver.onNext(getTasksResult.taskBundle)
                    responseObserver.onCompleted()
                }
            }
        }
    }

    override fun updateTask(
        request: TaskOuterClass.Task,
        responseObserver: StreamObserver<TaskOuterClass.TaskId>
    ) {
        withConn(pool) {
            val updateTaskResult = updateTask.process(conn, request)
            when (updateTaskResult.result) {
                TaskIdResult.INVALID_TOKEN -> responseObserver.onError(
                    Status
                        .PERMISSION_DENIED
                        .withDescription("Token or TaskId is invalid. Re-login or check TaskID")
                        .asException()
                )
                TaskIdResult.MISSING_FIELDS -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("Not are fields are filled out. " +
                            "As of now, the fields [task], must be filled out.")
                        .asException()
                )
                TaskIdResult.INVALID_TASK_ID -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("Task ID is invalid or corrupt.")
                        .asException()
                )
                TaskIdResult.OK -> {
                    responseObserver.onNext(updateTaskResult.task)
                    responseObserver.onCompleted()
                }
            }
        }
    }

    override fun deleteTask(
        request: TaskOuterClass.TaskId,
        responseObserver: StreamObserver<TaskOuterClass.TaskDeleteResponse>
    ) {
        withConn(pool) {
            val deleteTaskResult = deleteTask.process(conn, request)
            when (deleteTaskResult.result) {
                DeleteTaskResult.INVALID_TOKEN -> responseObserver.onError(
                    Status
                        .PERMISSION_DENIED
                        .withDescription("Token or TaskId is invalid. Re-login or check TaskID")
                        .asException()
                )
                DeleteTaskResult.INVALID_TASK_ID -> responseObserver.onError(
                    Status
                        .INVALID_ARGUMENT
                        .withDescription("TaskID is invalid. Please ensure it is correct.")
                        .asException()
                )
                DeleteTaskResult.OK -> {
                    responseObserver.onNext(TaskOuterClass.TaskDeleteResponse.getDefaultInstance())
                    responseObserver.onCompleted()
                }
            }
        }
    }
}
