package yaklet.webserver.endpointController.authService

import dev.yakstack.transport.Login
import org.jooq.impl.DSL.field
import org.jooq.impl.DSL.table
import yaklet.webserver.dbConn.DB_SESSIONS_TB
import yaklet.webserver.dbConn.DbConn
import java.lang.IllegalArgumentException
import java.util.*
import java.util.logging.Logger

class LogoutController {
    companion object {
        private val LOGGER = Logger.getLogger(this::class.java.name)
        private const val TIME_TO_SLEEP = 100L
    }

    fun process(conn: DbConn, token: Login.Token): LogoutResultResult {
        try {
            endSession(conn, token)
        } catch (_: IllegalArgumentException) {
            return LogoutResultResult((LogoutResult.INVALID_TOKEN))
        }
        return LogoutResultResult(LogoutResult.OK)
    }

    private fun endSession(conn: DbConn, token: Login.Token) {
        conn.sql.deleteFrom(table(DB_SESSIONS_TB))
            .where(field("token").eq(UUID.fromString(token.token)))
            .execute()
    }
}

enum class LogoutResult {
    INVALID_TOKEN,
    OK
}

data class LogoutResultResult(val result: LogoutResult)

