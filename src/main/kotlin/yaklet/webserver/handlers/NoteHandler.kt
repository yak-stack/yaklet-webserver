package yaklet.webserver.handlers

import yaklet.webserver.config.MAX_NOTE_SIZE

class NoteHandler {
    // Ensure the user does not submit notes greater than 1024 characters
    fun truncateNotes(notes: String): String {
        return if (notes.length > MAX_NOTE_SIZE) {
            notes.substring(0, MAX_NOTE_SIZE)
        } else  {
            notes
        }
    }

}
