package yaklet.webserver.server

import io.grpc.Server
import io.grpc.ServerBuilder
import io.grpc.protobuf.services.ProtoReflectionService
import yaklet.webserver.config.DEFAULT_TIMEOUT
import yaklet.webserver.config.SERVING_PORT
import yaklet.webserver.dbConn.ConnectionPool
import yaklet.webserver.transport.AuthService
import yaklet.webserver.transport.TaskService
import yaklet.webserver.transport.TimeCommitService
import java.io.File
import java.util.logging.Level
import java.util.logging.Logger

// Accept config later for configuring parameters in the Defaults.kt file
class Server(private val pool: ConnectionPool, certChain: String?, privateKey: String?) {
    companion object {
        private val LOGGER = Logger.getLogger(this::class.java.name)
    }

    val server = constructServer(certChain, privateKey)
    fun start() {
        server.start()
        LOGGER.log(Level.INFO, "Tracking time now ⏲️  on port: $SERVING_PORT")
        Runtime.getRuntime().addShutdownHook(Thread {
            stop()
        })
        server.awaitTermination()
    }

    private fun stop() {
        LOGGER.log(Level.INFO, "Yaklet Webserver received shutdown signal and is exiting.")
        pool.shutdown(DEFAULT_TIMEOUT)
        server.shutdown()
    }

    private fun constructServer(certChain: String?, privateKey: String?): Server = ServerBuilder.forPort(SERVING_PORT)
        .apply {
            if (certChain != null && privateKey != null) {
                useTransportSecurity(File(certChain), File(privateKey))
                LOGGER.log(Level.INFO, "Running with authentication (TLS)")
            } else {
                LOGGER.log(Level.WARNING, "Running without authentication. " +
                    "This should only be used in development environments.")
            }
        }
        .addService(ProtoReflectionService.newInstance())
        .addService(AuthService(pool))
        .addService(TaskService(pool))
        .addService(TimeCommitService(pool))
        .build()
}

